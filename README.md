LOop-Tree dualiTY automation (Lotty)
====================================

![Lotty Logo](lotty-logo.jpg)

***

`Lotty` is a Mathematica package that automates the novel
formulation of the loop-tree duality theorem together
with the manifestly causal representation of scattering amplitudes. 
References to this new formulation are addressed below.

The general concepts and algorithms implemented in `Lotty`
are described in the [paper](https://arxiv.org/abs/2103.09237).


## Author

William J. Torres Bobadilla	(Max Planck Institute For Physics)


***
## Installing Lotty

`Lotty` is a standalone package that only requires downloading
the main source from this repository.

In order to take advantage of all features of `Lotty`,
including parallelisation, 
one needs to define the variable `$LottyPath`
with the path to `Lotty`. 
For instance, typing the following lines in the `init.m` file of mathematica,

```
(* Lotty package *)
$LottyPath = "/lotty/install/path"
If[Not[MemberQ[$Path,$LottyPath]],$Path = Flatten[{$Path, $LottyPath}]];
```

***
## Usage

Together with this package, it is also provided a Mathematica notebook,
`usage.nb`, with applications of dual and causal integrand 
of multi-loop Feynman integrands.

A numerical study and integration of 
two-loop planar and non-planar scalar triangle is provided in
the Mathematica notebook `LTD_integrations.wl`. 

***
## References

1. J. J. Aguilera-Verdugo, F. Driencourt-Mangin,
R. J. Hernandez-Pinto, J. Plenter, S. Ramirez-Uribe,
A. E. Renteria-Olivo, G. Rodrigo, G. F. R. Sborlini, W. J. Torres
Bobadilla, S. Tracz,
_Open loop amplitudes and causality to all orders and powers from the
loop-tree duality_,
Phys. Rev. Lett. __124__ (2020) 211602,
[2001.03564](https://arxiv.org/abs/2001.03564).

2. J. J. Aguilera-Verdugo, R. J. Hernandez-Pinto, G. Rodrigo, G. F. R. Sborlini, W. J. Torres Bobadilla,
_Causal representation of multi-loop Feynman integrands within the
loop-tree duality_,
JHEP __01__ (2021) 069,
[2006.11217](https://arxiv.org/abs/2006.11217).

3. S. Ramirez-Uribe, R. J. Hernandez-Pinto, G. Rodrigo,
G. F. R. Sborlini, W. J. Torres Bobadilla,
_Universal opening of four-loop scattering amplitudes to trees_,
JHEP __04__ (2021) 129,
[2006.13818](https://arxiv.org/abs/2006.13818).

4. J. J. Aguilera-Verdugo, R. J. Hernandez-Pinto, G. Rodrigo, G. F. R. Sborlini, W. J. Torres Bobadilla,
_Mathematical properties of nested residues and their application to
multi-loop scattering amplitudes_,
JHEP __02__ (2021) 112,
[2010.12971](https://arxiv.org/abs/2010.12971).

5. W. J. Torres Bobadilla,
_Loop-tree duality from cusps and edges_,
JHEP __04__ (2021) 183,
[2102.05048](https://arxiv.org/abs/2102.05048).

6. W. J. Torres Bobadilla,
_Lotty -- The loop-tree duality automation_,
Accepted for publication in EPJC,
[2103.09237](https://arxiv.org/abs/2103.09237).
