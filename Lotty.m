(* ::Package:: *)

$LottyParallel::usage = "Parallelises Lotty. This function has to be set to True or False before loading Lotty.";
$LottyKernels::usage = "Sets the number of kernels where Lotty is going to run.";


BeginPackage["Lotty`"];


(* Lotty residue *)
ResidueW::usage = "\
ResidueW[expr,{z,z0}] in-house implementation of the function built-in function Residue.";
GetDual::usage = "GetDual[numerator,propagators,LoopMom] gets the multi-loop LTD \
representation of an integrand with a numerator in terms of \
\!\(\*SuperscriptBox[SubscriptBox[\(q\), \(i, 0\)], \((+)\)]\), a list of \
propagators and a list of loop momenta. External momenta are not included by \
default, but they can be included in the option \"Assumptions\". \
Also, if one does not want to keep track of all Heaviside theta functions, \
set the function \"KeepHeavisideThetas\"\[Rule]True. \
All propagators in the integrand are considered by default as linear ones, \
but they powers can be raised with the option \"Powers\"\[Rule]Automatic.";

RefineDual::usage = "\
RefineDual[expr,numerator,propagators,LoopMom] does reverse engineering \
and provides each residue of the multi-loop dual representation in terms \
of GD[i,j,k,...]";
GetCausalProps::usage = "\
GetCausalProps[expr,propagators] finds all causal propagators of \
the topology under study.";
LL::usage = "\
LL[sgn][{i1,i2,...}] represents a combination of causal propagators with \
sign sg = \"p\" or \"m\".";
RefineLambda::usage = "\
RefineLambda[nn] applies momentum conservation to the subsets of \
external momentum.";
AllCausal::usage = "AllCausal[nn] \
generates the parametric formula in terms of LL or \[Lambda] of the most general \
loop topology with nn cusps. \
The former is the default value of \"ExpandToLambda\"\[Rule]False, \
whereas the latter is activated with \"ExpandToLambda\"\[Rule]True";
Lamb2qij::usage = "\
Lamb2qij[expr,topology] gives a replacement list from \[Lambda][{i,j,...}] to q[{i,j,...}] \
where notice that {i,j,...} are the vertices (or external momenta attached to the vertices). \
Topology must be written in terms \[Lambda][sg][{i,j,...}] \
and topology should give the shape of the topology e.g. \
q[{1}] = {1,2}, q[{2}] = {3,4}, ...";
PlotTop::usage = "\
PlotTop[expr] draws the loop topology from the definition \
of the adjancencies.";
GetDisCusps::usage = "GetDisCusps[SetSingLam] Displays the cusps that are disconnected. \
SetSingLam corresponds to the edges that connect to a cusps, e.g. q[{1}]->{1,2} .";
RefineCausal::usage = "RefineCausal[expr,SetSingLam] \
simplifies the structure of L[sg][{i,j,...}] in term of products, e.g. \
L[sg][{i,j,k,l}] -> L[sg][{i,j,k}]L[sg][{l}] \ 
meaning that Intersection[{l},{i,j,k}] = 0.";
FromSP2q0::usage = "FromSP2q0[{{q1,m1},{q2,m2},...,{qN,mN}},{l1,l2,...,L}] \
expresses scalar products of loop momenta in terms of \
\!\(\*SuperscriptBox[SubscriptBox[\(q\), \(i, 0\)], \((+)\)]\) and external kinematics."


(* auxiliary functions *)
TableW::usage = "Parallelises or does not parallelise Lotty.";
MapW::usage = "Parallelises or does not parallelise Lotty.";
sp::usage = "sp[a,b] (d-1)-dimensional Euclidean scalar product.";
mp::usage = "mp[a,b] d-dimensional Minkowskian scalar product.";


(* Lotty integration *)
LoopToSC::usage = "\
LoopToSC[LoopMom,dim] writes integrand in spherical coordinates, in a dim*(L-1) sphere. \
LoopMom is the list of the loop momenta";
meassure::usage = "\
meassure[LoopMom,dim] corresponds to the integration meassure in dim*(L-1) dimensions. \
LoopMom is the list of the loop momenta";
intvars::usage = "\
intvars[LoopMom,dim] writes integration variables with integration limits. \
LoopMom is the list of the loop momenta";


(* names for the several outputs *)
q::usage  = "labels for the internal lines.";
p::usage  = "labels for the external momenta & cusps.";
L::usage  = "labels combinations of causal propagators." ;
GD::usage = "label the residues in the dual representation.";
\[Lambda]::usage = "label for causal threshold.";
\[Lambda]2qi0::usage = "causal thresholds to explicit representation.";
causal2\[Lambda]::usage = "converts sums of qi0 in \[Lambda].";


(* $LottyFunctions *)
$LottyFunctions::usage = "Collects all functions used in this package.";
$LottyVersion::usage = "Lotty's version";


Begin["`Private`"];


$LottyFunctions = {
ResidueW,GetDual,RefineDual,GetCausalProps,LL,AllCausal,
LoopToSC,meassure,intvars,LL,GetDisCusps,FromSP2q0,
TableW,MapW
};


Subscript[a_Plus,0]:=(Subscript[#,0]&/@a)
Subscript[Times[-1,a_],0]:=-Subscript[a,0];


Subsuperscript[a_Plus,0,"(+)"]:=(Subsuperscript[#,0,"(+)"]&/@a)
Subsuperscript[Times[-1,a_],0,"(+)"]:=-Subsuperscript[a,0,"(+)"];


Options[ResidueW] = {"Debugging"->False};
ResidueW[expr_,res_,OptionsPattern[]]:=Module[
{deno,denolist,posexp,exp,tmp},
deno = expr//Together//Denominator;
denolist = FactorList[deno];
posexp = denolist/.(res/.List->Rule)//Position[#,0,2]&//ReplaceAll[{a_,1}:>{a,2}];
If[OptionValue["Debugging"],Print["Factor list :: ",FactorList[deno]]];
If[OptionValue["Debugging"],Print["Deno list :: ",denolist/.(res/.List->Rule)]];

If[Length[posexp]==0,Return[0],exp = denolist//Extract[#,posexp]&//Extract[#,1]&];
If[OptionValue["Debugging"],Print["exp = ",exp]];
tmp = D[Power[(res[[1]]-res[[2]]),exp]expr//Together,
{res[[1]],exp-1}];
If[exp>1,tmp = tmp//Together];
tmp = 1/Factorial[exp-1]ReplaceAll[tmp,(res/.List->Rule)];
Return[tmp];
]


Options[CheckOnHeaviside] = {"Debugging"->False};
CheckOnHeaviside[expr_List,OptionsPattern[]]:=Module[
{tmp,theta},
theta =expr//Total//Variables//Cases[_HeavisideTheta];
tmp = Coefficient[expr,#]&/@theta;
(*tmp = tmp/.Power[Subsuperscript[q[i_],__],-1]:>delta[q[i]]//.delta[x__]delta[y__]:>delta[x,y];*)
tmp = tmp/.Power[Subsuperscript[q[i_],b__],nn_]/;nn<0:>delta[q[i]]Power[Subsuperscript[q[i],b],nn+1]//.delta[x__]delta[y__]:>delta[x,y];
tmp = MapW[Composition[Collect[#,_delta,Together]&,Total],tmp]//Total;
If[OptionValue["Debugging"],Print[tmp]];
If[tmp===0||tmp==={},Return[True],Print["ALL HEAVISIDE THETAS WERE NOT CANCELED"]];
]


Options[GetDual]={"Debugging"->False,"Assumptions"->{},"KeepHeavisideThetas"->False,"Powers"->Automatic};
GetDual[num_,props_List,LoopMom_List,OptionsPattern[]]:=Module[
{assumptions1,assumptions2,assumptions,deno,int,L1,sol1,zeroes,pos0,dummy,i,mypowers},

(* Generate assumptions *)
assumptions1 = Table[Im[Subsuperscript[q[i],0,"(+)"]]<0,{i,Length@props}];
assumptions2 = (Im[Subscript[LoopMom[[#]],0]]==0)&/@Range[Length@LoopMom];
assumptions = Join[assumptions1,assumptions2];
assumptions = Join[assumptions,OptionValue["Assumptions"]]//Flatten;

mypowers = If[OptionValue["Powers"]===Automatic,
Table[1,Length[props]],
OptionValue["Powers"]
];

If[Length[mypowers] != Length[props],
Print["# POWERS \[NotEqual] # PROPAGATORS"];
Return[]];

(* Build denominator *)
deno = Times@@Table[
Power[(q0[i]-q0plus[i])(q0[i]+q0plus[i]),mypowers[[i]]],
{i,Length@props}
];
deno = deno/.{q0[i_]:>Subscript[props[[i]],0],q0plus[i_]:>Subsuperscript[q[i],0,"(+)"]};
int = num/deno//List;

(* Start calculation of residues *)
Monitor[
For[L1=1,L1<=Length[LoopMom],L1++,
deno = Times@@Denominator[int];
deno = deno//FactorList;
deno = deno[[All,1]]//DeleteCases[#,_Integer|Subsuperscript[q[_],0,"(+)"]]&;
sol1 =deno//Solve[#==0,Subscript[LoopMom[[L1]],0]]&/@#&//Flatten//Values//DeleteDuplicates;

If[OptionValue["Debugging"],Print["SOLUTION -> CHECK"]];
zeroes = Composition[HeavisideTheta,Im][-#]&/@sol1//Simplify[#,Assumptions->assumptions]&/@#&;
pos0 = zeroes//Position[#,0,1]&;
sol1 = sol1//Delete[#,pos0]&;
zeroes = zeroes//Delete[#,pos0]&;

If[OptionValue["Debugging"],Print["COMPUTING LOOP ",L1]];

DistributeDefinitions[sol1,int,zeroes,LoopMom,L1];
int = TableW[
(*Print["COMPUTING ",{L1,i,j}];*)
zeroes[[i]]ResidueW[int[[j]],{Subscript[LoopMom[[L1]],0],sol1[[i]]}]
,{i,Length@sol1},{j,Length@int}
]//Flatten(*//Total*);

If[OptionValue["KeepHeavisideThetas"],
If[OptionValue["Debugging"],Print["SORTING HEAVISIDE THETAS "]];

If[CheckOnHeaviside[int],
int = int/._HeavisideTheta->0//DeleteCases[#,0,{1}]&,
Break[]],
int = int/._HeavisideTheta->0//DeleteCases[#,0,{1}]&;
];
If[Not[$Notebooks],PrintTemporary["RESIDUE ",L1," COMPUTED"]];
],
ProgressIndicator[L1,{1,Length@LoopMom}]]//Quiet;
Return[int];
]


Options[RefineDual] = {"Debugging"->False,"GetAllOptions"->False,"Powers"->Automatic};
RefineDual[expr_List,num_,props_List,LoopMom_List,OptionsPattern[]]:=Module[
{deltas,repl,fullampl,sys,deno,int,int2,loopmom,terms,pows,repl1},

pows = If[OptionValue["Powers"]===Automatic,Table[1,Length@props],OptionValue["Powers"]];

deltas = expr/.Power[Subsuperscript[q[i_],0,"(+)"],nn_]/;nn<0:>Delta[q[i]]//.Delta[x__]Delta[y__]:>Delta[x,y];
deltas = deltas/.q[a_]:>a//occur[#,_Delta]&/@#&//ReplaceAll[Delta->List]//Flatten[#,1]&;

DistributeDefinitions[deltas,pows];
If[OptionValue["Debugging"],Print["READING POWERS :: ",pows]];
fullampl = TableW[
(* build system *)
sys = Table[
(q0[i]-q0plus[i]),
{i,deltas[[j]]}
];
sys = sys//.{q0[a_]:>Subscript[props[[a]],0],q0plus[a_]:>Subscript[q[a],0]};
repl = Thread[sys==0]//Solve[#,Subscript[#,0]&/@LoopMom][[1]]&;
(* write integrands *)

deno = Times@@Table[
Power[(q0[i]-q0plus[i])(q0[i]+q0plus[i]),pows[[i]]],
{i,Length@props}
];
deno = deno/.{q0[i_]:>Subscript[props[[i]],0],q0plus[i_]:>Subsuperscript[q[i],0,"(+)"]};
int = num/deno//List;

(* Extract all residues *)
int2 = int/.repl;
loopmom = deltas[[j]];
terms = {{}};

For[i1=1,i1<=Length[LoopMom],i1++,
terms = Table[
Append[#,j1*loopmom[[i1]]]&/@terms,
{j1,{-1,1}}
]//Flatten[#,1]&;
int2 = Table[
j1*ResidueW[#,{Subscript[q@loopmom[[i1]],0],j1*Subsuperscript[q@loopmom[[i1]],0,"(+)"]}]&/@int2,
{j1,{-1,1}}]//Flatten;
];

repl1 = Table[Subsuperscript[q[ii],0,"(+)"]->RandomInteger[{10^3,10^4}]/RandomInteger[{10^4,10^3}],{ii,Length@pows}];
repl1 = Append[repl1,_Subscript->0];

int2 = int2-expr[[j]]/.repl1//MapW@Together;
int2 = int2//Together//Position[#,0,1]&;

If[OptionValue["Debugging"],PrintTemporary["EXTRACTED ALL RESIDUES FOR ",deltas[[j]]]];
terms//Extract[#,int2]&
,
{j,Length@deltas}];

If[OptionValue["Debugging"],PrintTemporary["ALL RESIDUES FOUND"]];
fullampl = Map[SortBy[#,Abs[#]&]&,fullampl,{2}];
fullampl = Map[GD,fullampl,{2}];
fullampl = If[OptionValue["GetAllOptions"],fullampl,fullampl[[All,1]]];

Return[fullampl];

];


Options[GetCausalProps] = {"GetPref"->False,"ExternalMomenta"->Automatic,"Powers"->Automatic};
GetCausalProps[expr_,props_,OptionsPattern[]]:=Module[
{tmp,mypref,amptest,XX,prepl,sub1,sub2,sub,pows},

pows = If[TrueQ[OptionValue["Powers"]==Automatic],1,OptionValue["Powers"]];

prepl = If[TrueQ[OptionValue["ExternalMomenta"]==Automatic],
{Subscript[__]:>0},
OptionValue["ExternalMomenta"]
];

tmp = expr;
mypref = Subsuperscript[q@#,0,"(+)"]&/@Range[props//Length];
mypref = Times@@((2*mypref)^pows);

If[OptionValue["GetPref"],Return[mypref]];

amptest = tmp*mypref/.prepl//Map[Cancel];
amptest = amptest//Denominator//Map[FactorList]//Flatten//DeleteDuplicates//DeleteCases[#,_Integer]&//ReplaceAll[-Subsuperscript[__]:>XX];
amptest = amptest/.a_*XX:>XX//DeleteCases[#,XX+__]&//SortBy[#,Length]&;

sub1 = Thread[amptest->(\[Lambda]/@Range[Length@amptest])];
sub2 = Thread[-amptest->(-\[Lambda]/@Range[Length@amptest])];
sub = Union[sub1,sub2];
\[Lambda]2qi0 = sub1//Map@Reverse;
causal2\[Lambda] = sub;

amptest = tmp*mypref//Map[Cancel];
amptest = amptest/.sub//Denominator//Map[FactorList]//Flatten//DeleteDuplicates//DeleteCases[#,_Integer]&//ReplaceAll[-Subsuperscript[__]:>XX];
amptest = amptest/.a_*XX:>XX//DeleteCases[#,XX+__]&//Sort;

Print["ALL \[Lambda][i] ARE STORED IN THE FUNCTION \[Lambda]2qi0"];
Return[amptest];
];


LL[sg_][a_]/;SameQ[Length[a],0]:=1;
LL[sg_][a_]/;SameQ[Length[a],1]:=1/\[Lambda][sg][a];
LL[sg_][a_List]:=1/\[Lambda][sg][a] Total[LL[sg][#]&/@Subsets[a,{-1+Length@a}]];


RefineLambda[n_]:=Module[
{tmp1,tmp2,all,repl},
tmp1 =  Range[n]//Subsets[#,IntegerPart[n/2]]&//DeleteCases[a_/;Length[a]<=1];
tmp2 =  Range[n-1]//Subsets[#,IntegerPart[n/2]]&//DeleteCases[a_/;Length[a]<=1];
tmp1 = Complement[tmp1,tmp2];
tmp2 = Complement[Range[n-1],#]&/@tmp1;
repl = Join[Thread[(\[Lambda]["+"][#]&/@tmp1)->(\[Lambda]["-"][#]&/@tmp2)],
Thread[(\[Lambda]["-"][#]&/@tmp1)->(\[Lambda]["+"][#]&/@tmp2)]];
Return[repl];
]


Options[AllCausal] = {"ExpandToLambda"->False};
AllCausal[nn_,OptionsPattern[]]:=Module[
{n1,n2,lamb,Lp,Lm,full},
n1 = IntegerPart[(nn-2)/2]+1;
n2 = IntegerPart[(nn-2)]-IntegerPart[(nn-2)/2];
lamb = Range[nn];
Lp = Subsets[lamb,List@n1];
Lm = Complement[lamb,#]&/@Lp;
Lm = Subsets[#,List@n2]&/@Lm;
Lm = Total[#]&/@Map[L["-"][#]&,Lm,{2}];
Lp = Map[L["+"][#]&,Lp,{1}];
full = Thread[Times[Lp,Lm]]//Total;
full = If[OptionValue["ExpandToLambda"],
full/.L->LL/.If[EvenQ[nn],RefineLambda[nn],{}],full];
Return[full];
];


Lamb2qij[expr_,SetSingLam_]:=Module[
{myqs,SingLam,tmp,tmpI,tmpU,tmpT,repl,toq},

toq = {
\[Lambda]["+"][a__]:>q[a]+Total[Subscript[p[#],0]&/@a],
\[Lambda]["-"][a__]:>q[a]-Total[Subscript[p[#],0]&/@a]
};

myqs = expr/.toq//Variables//Cases[q[a_]/;Length[a]>1];
SingLam = SetSingLam;
repl = {};
For[i=1,i<=Length@myqs,i++,
tmp = myqs[[i]]/.q[a__]:>Map[q@List@#&,Subsets[a,{2}],{2}];
tmpI = Intersection@@@(tmp/.SingLam)//Flatten;
tmpU = Union@@@(tmp/.SingLam)//Flatten;
tmpT = Complement[tmpU,tmpI];
repl = Append[repl,tmpT];
];
repl = Thread[myqs->repl];
repl = Join[SetSingLam,repl]/.(a_->b_):>(a->Total[Subsuperscript[q[#],0,"(+)"]&/@b]);
repl = Join[toq,repl];
Return[repl];
]


PlotTop[SetSingLam_]:=Module[
{tmp},
tmp = SetSingLam/.q[{a_}]:>p@@{a}/.(a_->b_?ListQ):>Thread[a->q/@b]//Flatten;
tmp = tmp//.{a___,a1_->q[x1_?IntegerQ],b___,a2_->q[x1_?IntegerQ],c___}:>{Labeled[a1<->a2,Subsuperscript[q[x1],0,"(+)"]],a,b,c};
tmp = tmp//Graph[#,VertexLabels->"Name",VertexSize->Small,VertexStyle->Red,BaseStyle->Blue,EdgeStyle->Thick,EdgeLabelStyle->Directive[Blue,Bold,15,Background->White],
VertexLabelStyle->Directive[Red,Bold,15,Background->White],
GraphLayout->"SpringElectricalEmbedding"]&;
Return[tmp];
];


checkW[sets__]:=Module[
{tmp},
tmp = Intersection[Sequence@@#]&/@Subsets[sets,{2}]//Flatten//Union;
tmp = If[SameQ[tmp,{}],True,False];
Return[tmp];
];


GetDisCusps[SetSingLam_]:=Module[
{nn,maxgroup,DiscVert,tmp,tmp1,i},
nn = Length[SetSingLam];
maxgroup  = IntegerPart[nn/2];
DiscVert = {};
For[i=maxgroup,i>=2,i--,
tmp = q/@List/@Range[nn]//Subsets[#,{i}]&;
tmp1 = tmp/.SetSingLam//Map@checkW//Position[True];
tmp = Extract[tmp,tmp1]/.q[{a_}]:>a;
DiscVert = Union[DiscVert,tmp];
];
Return[DiscVert];
];


RefineCausal[expr_,SetSingLam_]:=Module[
{lhs,rhs,sub},
lhs = expr//Variables//Cases[L[_][a_]/;Length[a]>1];
rhs = lhs//GetLs[#,SetSingLam]&/@#&;
sub = Thread[lhs->rhs];
Return[expr/.sub];
]


GetLs[expr_,SetSingLam_]:=Module[
{tmp,myvtx,qs,qset,qset1,qsetC,qsetC1,pos,inter},
myvtx = expr/.L[sg_][vtx_]:>vtx;
qs = q/@List/@myvtx;
qset = qs//Subsets[#,{1,IntegerPart[Length[myvtx]/2]}]&;
qset1 = Union@@@(qset/.SetSingLam);
qsetC = Complement[qs,#]&/@qset;
qsetC1 = Union@@@(qsetC/.SetSingLam);
pos = Thread[inter[qset1,qsetC1]]/.inter->Intersection//Position[{}];
tmp = If[SameQ[pos,{}],expr,expr/.L[sg_][vtx_]:>L[sg][#]&/@(Extract[#,First[pos]]&/@{qset,qsetC}/.q[{a_}]:>a)//Apply[Times]];
Return[tmp];
]


occur[expr_,pattern_]:=expr//Position[#,pattern]&//Extract[expr,#]&//Complement


Options[FromSP2q0] = {"Kinematics"->{},"ApplyFunction"->Together,"AllqToLoopMom"->True};
FromSP2q0[props_List,LoopMom_List,OptionsPattern[]]:=Module[
{momenta,momentaSq,masses,denos,patt,sol,subs,fun,d},
subs = OptionValue["Kinematics"];
fun = OptionValue["ApplyFunction"];
momenta = props//Extract[#,1]&/@#&;
momentaSq = momenta//mp/@#&;
masses = props//Extract[#,2]&/@#&//Power[#,2]&;
denos = momentaSq - masses;
patt = Alternatives@@(mp[#,_]&/@LoopMom);

sol = Thread[d/@Range[Length@momenta]==denos]//Solve[#,occur[#,patt]][[1]]&;
sol = sol/.subs/.d[i_]:>Subscript[q[i],0]^2-Subsuperscript[q[i],0,"(+)"]^2;
sol = If[OptionValue["AllqToLoopMom"],
sol/.Subscript[q[i_],0]:>Subscript[momenta[[i]],0]//fun,
sol//fun];
sol = sol/.subs;
Return[sol];
]


(* routines for integration *)


Attributes[sp] = {Orderless};
sp[p1_] := sp[p1,p1];
sp[p1_Plus, p2_] := (sp[#1, p2]&)/@p1;
sp[0, p1_] := 0;
sp[-(k_), x_] := -sp[k, x];
sp[a_List,b_List]:=a.b;


Attributes[mp] = {Orderless};
mp[p1_] := mp[p1,p1];
mp[p1_Plus, p2_] := (mp[#1, p2]&)/@p1;
mp[0, p1_] := 0;
mp[-(k_), x_] := -mp[k, x];
mp[a_List,b_List]:=a[[1]]*b[[1]]-a[[2;;]].b[[2;;]];


(* nn dimensional shperical coordinates *)
myvars[nn_]:=Flatten@List[myvars[nn-1][[;;-2]],myvars[nn-1][[-1]]Cos[\[Theta][nn-1]],myvars[nn-1][[-1]]Sin[\[Theta][nn-1]]];
myvars[2] = r*List[Cos[\[Theta][1]],Sin[\[Theta][1]]];


(* integration meassure*)
Options[meassure] = {"Domain"->"Sphere"};
meassure[L_]:=meassure[L]=meassure[L-1]*r*Product[Sin[\[Theta][i]],{i,L-2}]
meassure[2]:=r
(**)
meassure[LoopMom_,dim_,OptionsPattern[]]:=Module[
{jac,vars,L1},

L1 = Length[LoopMom];

If[SameQ[OptionValue["Domain"],"HyperSphere"],
jac = meassure[(dim-1)L1];
jac = jac/.\[Theta][i_]:>ToExpression["\[Theta]"<>ToString[i]];
jac = jac/.r:>ToExpression["r"<>ToString[1]];
];

If[SameQ[OptionValue["Domain"],"Sphere"],
vars = {r->r[#],\[Theta][a_]:>\[Theta][#,a]}&/@Range[L1];
jac = meassure[(dim-1)]/.vars;
jac = Times@@jac;
jac = jac/.\[Theta][i_,j_]:>ToExpression["\[Theta]"<>ToString[i]<>ToString[j]];
jac = jac/.r[i_]:>ToExpression["r"<>ToString[i]];
];

Return[jac];
];


(* loop momenta to spherical coordinates *)
(*dim = 1 + spatial*)
Options[LoopToSC] = {"Domain"->"Sphere"};
LoopToSC[LoopMom_List,dim_,OptionsPattern[]] := Module[
{L1,ToSC},
L1 = Length[LoopMom];

If[SameQ[OptionValue["Domain"],"HyperSphere"],
ToSC = Thread[LoopMom->ArrayReshape[myvars[(dim-1)*L1],{L1,dim-1}]];
ToSC = ToSC/.\[Theta][i_]:>ToExpression["\[Theta]"<>ToString[i]];
ToSC = ToSC/.r:>ToExpression["r"<>ToString[1]];
];

If[SameQ[OptionValue["Domain"],"Sphere"],
ToSC = LoopMom[[#]]->(myvars[(dim-1)]/.r->r[#]/.\[Theta][a_]:>\[Theta][#,a])&/@Range[L1];
ToSC = ToSC/.\[Theta][i_,j_]:>ToExpression["\[Theta]"<>ToString[i]<>ToString[j]];
ToSC = ToSC/.r[i_]:>ToExpression["r"<>ToString[i]];
];

Return[ToSC];
];


(* integration vars *)
Options[intvars] = {"Domain"->"Sphere","MaxValueR"->10^5};
intvars[LoopMom_,dim_,OptionsPattern[]]:=Module[
{vars,rr,L1},
rr = OptionValue["MaxValueR"];
L1 = Length[LoopMom];

If[SameQ[OptionValue["Domain"],"HyperSphere"],
vars = Join[List[{r,0,rr}],Table[{\[Theta][i],0,\[Pi]},{i,(dim-1)L1-2}],List[{\[Theta][(dim-1)L1-1],0,2\[Pi]}]];
vars = vars/.{r->ToExpression["r"<>ToString[1]],\[Theta][i_]:>ToExpression["\[Theta]"<>ToString[i]]};
];

If[SameQ[OptionValue["Domain"],"Sphere"],
vars = Table[
Join[List[{r[j],0,rr}],Table[{\[Theta][j,i],0,\[Pi]},{i,(dim-1)-2}],List[{\[Theta][j,(dim-1)-1],0,2\[Pi]}]],
{j,L1}];
vars = vars/.\[Theta][i_,j_]:>ToExpression["\[Theta]"<>ToString[i]<>ToString[j]];
vars = vars/.r[i_]:>ToExpression["r"<>ToString[i]];
vars = vars//Flatten[#,1]&//Sort;
];
Return[vars];
]


$LottyVersion = "(March 14th of 2021)";
End[];


If[$Notebooks,
Print[
"------------------------------------------------------------",
Style["
Lotty -- the LOop-Tree dualiTY automation",14,Black],
Style["
\tby William J. Torres Bobadilla (MPP)",12,Black],
"\n\t\tVersion 1.0 "<>$LottyVersion,
"\nAll functions are stored in the variable $LottyFunctions
------------------------------------------------------------"
];
];                        


EndPackage[]


If[TrueQ[$LottyParallel],
LaunchKernels[$LottyKernels];
TableW = ParallelTable;
MapW = ParallelMap;
ParallelEvaluate[Import@FileNameJoin[{$LottyPath,"Lotty.m"}]];
Print["All evaluations in Lotty will be performed in ",$LottyKernels," kernels"],
TableW = Table;
MapW = Map;
];
