(* ::Package:: *)

CloseKernels[];
Quit[];


$LottyParallel = True;
$LottyKernels  = 2$ProcessorCount;
<<Lotty`


(* ::Section:: *)
(*3pt functions*)


energies = {Subscript[p[1],0]->Ecm/2, Subscript[p[2],0]->Ecm/2};
spatial  = {p[1]->Ecm/2 {1,0,0}, p[2]->Ecm/2 {-1,0,0}};
dim = 4;


mypts =  Table[List[Ecm->kk/4*jj],{jj,2,8,2},{kk,{I,1}}]//Flatten[#,1]&//Sort;
mypts = mypts/.{2->5/4};


(* ::Subsection:: *)
(*one-loop*)


num = 1;
props = {l1,l1+p[1],l1+p[1]+p[2]};
LoopMom = {l1};
assumptions = (Im[Subscript[p[#],0]]==0)&/@Range[2];


tmp = GetDual[num,props,LoopMom,"Assumptions"->assumptions];
tmp1 = GetCausalProps[tmp,props,"GetPref"->True];
tmp2 = tmp1*tmp//Total;


SetSingLam = {
q[{1}] -> {1,2},
q[{2}] -> {2,3},
q[{3}] -> {3,1}
};


PlotTop[SetSingLam]


momcon = {p[3]->-p[1]-p[2]};
tmp3 = AllCausal[3,"ExpandToLambda"->True];
toLambda = Lamb2qij[tmp3,SetSingLam];
tmp4 = tmp3//.toLambda/.momcon


tmp2-tmp4//Together


myqi0 = Subsuperscript[q[#],0,"(+)"]&/@Range[Length@props];
value = myqi0/.Subsuperscript[q[ii_],__]:>Sqrt[sp@props[[ii]]+1]/.LoopToSC[LoopMom,4]/.spatial//Simplify;
myrepl = Thread[myqi0->value]


ClearAll[myint,mypref]
mypref[{q1_,q2_,q3_}]:=1/tmp1/.Subsuperscript[q[ii_],__]:>ToExpression["q"<>ToString[ii]]//Evaluate
myint[Ecm_,{q1_,q2_,q3_}]:=(
tmp4/.energies/.Subsuperscript[q[ii_],__]:>ToExpression["q"<>ToString[ii]]//Evaluate
)
mycomp = {r1->(1-x1)/x1,\[Theta]12->0};
myreplC = myrepl/.mycomp;
meassureC = 1/x1^2 meassure[LoopMom,dim]/.mycomp;


eval = TableW[
mypt = mypts[[i]];
(
(2\[Pi])(-(1/(2\[Pi])^3))
*meassureC
*mypref[Values[myreplC]/.mypt]
*myint[mypt[[1,2]],Values[myreplC]/.mypt]
//Re//NIntegrate[#,
{x1,0,1},
{\[Theta]11,0,\[Pi]}
]&
),{i,Length@mypts}
];//AbsoluteTiming


eval


(* ::Subsection:: *)
(*two-loop planar*)


num = 1;
props = {l1,l1+p[1],l1+p[1]+p[2],l2,l2-p[1]-p[2],l1+l2};
LoopMom = {l1,l2};
assumptions = (Im[Subscript[p[#],0]]==0)&/@Range[2];


tmp = GetDual[num,props,LoopMom,"Assumptions"->assumptions];
tmp1 = GetCausalProps[tmp,props,"GetPref"->True];
tmp2 = tmp1*tmp//Total;


SetSingLam = {
q[{1}] -> {1,2},
q[{2}] -> {2,3},
q[{3}] -> {4,5},
q[{4}] -> {1,4,6},
q[{5}] -> {3,6,5}
};


PlotTop[SetSingLam]


momcon = {p[3]->-p[1]-p[2],Subscript[p[4|5],_]->0};
tmp3 = AllCausal[5]//RefineCausal[#,SetSingLam]&;
tmp3 = tmp3/.L->LL;
toLambda = Lamb2qij[tmp3,SetSingLam];
tmp4 = tmp3//.toLambda/.momcon;


(* numerical check *)
repl1 = (Subsuperscript[q[#],0,"(+)"]->RandomInteger[{10^3,10^4}]/RandomInteger[{10^4,10^5}])&/@Range[Length@props];
repl1 =Join[repl1,( Subscript[p[#],0]->RandomInteger[{10^3,10^4}]/RandomInteger[{10^4,10^5}] I)&/@Range[3]];
tmp2-tmp4/.momcon/.repl1//Factor


myqi0 = Subsuperscript[q[#],0,"(+)"]&/@Range[Length@props];
value = myqi0/.Subsuperscript[q[ii_],__]:>Sqrt[sp@props[[ii]]+m[ii]^2]/.LoopToSC[LoopMom,4]/.spatial//Simplify;
myrepl = Thread[myqi0->value]/.m[6]->0/._m->1//FullSimplify


ClearAll[myint,mypref]
mypref[{q1_,q2_,q3_,q4_,q5_,q6_}]:=1/tmp1/.Subsuperscript[q[ii_],__]:>ToExpression["q"<>ToString[ii]]//Evaluate
myint[Ecm_,{q1_,q2_,q3_,q4_,q5_,q6_}]:=(
tmp4/.energies/.Subsuperscript[q[ii_],__]:>ToExpression["q"<>ToString[ii]]//Evaluate
)
mycomp = {r1->(1-x1)/x1,r2->(1-x2)/x2,\[Theta]22->0};
myreplC = myrepl//.mycomp;
meassureC = 1/x1^2 * 1/x2^2 * meassure[LoopMom,dim]//.mycomp;


eval3 = TableW[
mypt = mypts[[i]];
(
2\[Pi]*(-(1/(2\[Pi])^3))^2
*meassureC
*mypref[Values[myreplC]/.mypt]
*myint[mypt[[1,2]],Values[myreplC]/.mypt]
//Re//NIntegrate[#,
{x1,0,1/2,1},{x2,0,1/2,1},
{\[Theta]11,0,\[Pi]/2,\[Pi]},{\[Theta]12,0,\[Pi]/2,\[Pi],3\[Pi]/2,2\[Pi]},{\[Theta]21,0,\[Pi]/2,\[Pi]}
]&
),{i,Length@mypts}
];//AbsoluteTiming


eval3//ScientificForm


(* ::Subsection:: *)
(*two-loop non-planar*)


num = 1;
props = {l1,l1+p[1],l2,l2-p[1]-p[2],l1+l2,-l1-l2+p[2]};
LoopMom = {l1,l2};
assumptions = (Im[Subscript[p[#],0]]==0)&/@Range[2];


tmp = GetDual[num,props,LoopMom,"Assumptions"->assumptions];
tmp1 = GetCausalProps[tmp,props,"GetPref"->True];
tmp2 = tmp1*tmp//Total;


SetSingLam = {
q[{1}] -> {1,2},
q[{2}] -> {5,6},
q[{3}] -> {3,4},
q[{4}] -> {6,2,4},
q[{5}] -> {1,3,5}
};


PlotTop[SetSingLam]


momcon = {p[3]->-p[1]-p[2],Subscript[p[4|5],_]->0};
tmp3 = AllCausal[5]//RefineCausal[#,SetSingLam]&;
tmp3 = tmp3/.L->LL;
toLambda = Lamb2qij[tmp3,SetSingLam];
tmp4 = tmp3//.toLambda/.momcon;


(* numerical check *)
repl1 = (Subsuperscript[q[#],0,"(+)"]->RandomInteger[{10^3,10^4}]/RandomInteger[{10^4,10^5}])&/@Range[Length@props];
repl1 =Join[repl1,( Subscript[p[#],0]->RandomInteger[{10^3,10^4}]/RandomInteger[{10^4,10^5}] I)&/@Range[3]];
tmp2-tmp4/.momcon/.repl1//Factor


myqi0 = Subsuperscript[q[#],0,"(+)"]&/@Range[Length@props];
value = myqi0/.Subsuperscript[q[ii_],__]:>Sqrt[sp@props[[ii]]+1]/.LoopToSC[LoopMom,4]/.spatial//Simplify;
myrepl = Thread[myqi0->value];


ClearAll[myint,mypref]
mypref[{q1_,q2_,q3_,q4_,q5_,q6_}]:=1/tmp1/.Subsuperscript[q[ii_],__]:>ToExpression["q"<>ToString[ii]]//Evaluate
myint[Ecm_,{q1_,q2_,q3_,q4_,q5_,q6_}]:=(
tmp4/.energies/.Subsuperscript[q[ii_],__]:>ToExpression["q"<>ToString[ii]]//Evaluate
)
mycomp = {r1->(1-x1)/x1,r2->(1-x2)/x2,\[Theta]22->0};
myreplC = myrepl/.mycomp;
meassureC = 1/x1^2 1/x2^2 meassure[LoopMom,dim]/.mycomp;


eval = TableW[
mypt = mypts[[i]];
(
(2\[Pi])(-(1/(2\[Pi])^3))^2
*meassureC
*mypref[Values[myreplC]/.mypt]
*myint[mypt[[1,2]],Values[myreplC]/.mypt]
//Re//NIntegrate[#,
(*{x1,0,1},{x2,0,1},
{\[Theta]11,0,\[Pi]},{\[Theta]12,0,2\[Pi]},{\[Theta]21,0,\[Pi]}*)
{x1,0,1/2,1},{x2,0,1/2,1},
{\[Theta]11,0,\[Pi]/2,\[Pi]},{\[Theta]12,0,\[Pi]/2,\[Pi],3\[Pi]/2,2\[Pi]},{\[Theta]21,0,\[Pi]/2,\[Pi]}
]&
),{i,Length@mypts}
];//AbsoluteTiming


eval
